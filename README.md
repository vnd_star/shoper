<!DOCTYPE html>
<html>
<head>
	<title>My Website</title>
	<style>
		/* Set the background color to black */
		body {
			background-color: #000000;
		}
		
		/* Style the input field */
		input[type="text"] {
			width: 50%;
			padding: 10px;
			margin: 10px;
			border: none;
			border-radius: 5px;
			font-size: 16px;
		}
		
		/* Style the button */
		button {
			background-color: #00FF00; /* Green button color */
			color: #FFFFFF; /* White text color */
			padding: 10px 20px;
			border: none;
			border-radius: 5px;
			cursor: pointer;
		}
		
		button:hover {
			background-color: #00CC00; /* Lighter green on hover */
		}
	</style>
</head>
<body>
	<h1>Welcome to My Website</h1>
	<p>This is a sample website with a black background.</p>
	
	<!-- Add a text input field and a button -->
	<form>
		<input type="text" id="username" placeholder="Enter your key">
		<button id="signin-btn">Sign In</button>
	</form>
	
	<script>
		// Get the input field and button elements
		const inputField = document.getElementById("username");
		const signinBtn = document.getElementById("signin-btn");
		
		// Add an event listener to the button
		signinBtn.addEventListener("click", function() {
			// Get the input value
			const inputValue = inputField.value.trim();
			
			// Check if the input value is "9thebestlike"
			if (inputValue === "9thebestlike") {
				// Redirect to the Discord link
				window.location.href = "https://discord.gg/dxqNhzs6Y9";
			} else {
				alert("Invalid username!");
			}
		});
	</script>
</body>
</html>